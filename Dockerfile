from rust:1.57.0 as builder

run cd /opt && USER=root cargo new --bin app
workdir /opt/app

run apt-get update
run apt install -y libgexiv2-dev
run rustup default stable
copy . .
run cargo build --release

from rust:1.57.0-slim
env ROCKET_ADDRESS=0.0.0.0
env ROCKET_PORT=3000
run apt-get update && apt install -y libgexiv2-dev && rm -rf /var/lib/apt/lists/*
workdir /opt/app
copy --from=builder /opt/app/target/release/tempest-tmp /opt/app/tempest-tmp
copy ./Rocket.toml /opt/app/Rocket.toml
copy ./ui ./ui
cmd /opt/app/tempest-tmp
