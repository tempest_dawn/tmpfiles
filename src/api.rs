use std::{env, fs};
use chrono::Duration;
use rocket::State;
use rocket::form::Form;
use rocket::fs::TempFile;
use rocket::http::{Cookie, CookieJar, Status};
use rocket::serde::{Deserialize, Serialize};
use rocket::serde::json::Json;
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

use crate::sessions::{Session, SessionId, Sessions};

#[derive(Deserialize)]
pub struct Login {
  username: String,
  password: String
}

#[derive(Serialize)]
pub struct UploadResult {
  file_path: String,
  retention_hours: u64
}

#[post("/login", data = "<login>")]
pub async fn login(cookies : &CookieJar<'_>, sessions : &State<Sessions>, login : Json<Login>) -> Result<Json<SessionId>, (Status, Json<&'static str>)> {
  let username = login.username.clone();
  let password = login.password.clone();

  let user_file = fs::read_to_string("./data/shadow").unwrap();
  let user_line = user_file.split('\n').find(|s| -> bool {
    s.starts_with(format!("{}:", username).as_str())
  });

  if user_line.is_none() {
    bcrypt::hash("garbage", 12).unwrap();
    return Err((Status::Unauthorized, Json("Invalid login")))
  }

  let hash = user_line.unwrap().split(':').nth(1).unwrap();

  let result = bcrypt::verify(password, hash).unwrap();

  if !result {
    return Err((Status::Unauthorized, Json("Invalid login")))
  }

  let create = sessions.create_session(username, Duration::hours(2));

  if let Some((session_id, _session)) = create {
    cookies.add(Cookie::new("session", session_id.to_string()));
    return Ok(Json(session_id));
  }

  return Err((Status::InternalServerError, Json("UUID collision")))
}

#[post("/logout")]
pub async fn logout(cookies : &CookieJar<'_>, sessions : &State<Sessions>) -> Result<Status, Status> {
  let session_id = cookies.get("session").unwrap().value().parse::<SessionId>().unwrap();
  sessions.remove_session(session_id);
  cookies.remove(Cookie::new("session", ""));

  Ok(Status::Ok)
}

#[post("/upload", data = "<file>")]
pub async fn upload(_session : Option<Session>, mut file : Form<TempFile<'_>>) -> Result<Json<UploadResult>, (Status, &'static str)>  {
  if _session.is_none() {
    return Err((Status::Unauthorized, "Must be logged in to upload"));
  }

  let _session = _session.unwrap();

  let prefix : String = thread_rng()
    .sample_iter(&Alphanumeric)
    .take(8)
    .map(char::from)
    .collect();

  if file.len() > crate::cleanup::MAX_SIZE as u64 {
    return Err((Status::PayloadTooLarge, "Uploaded file exceeds maximum size"));
  }

  {
    let metadata_res = rexiv2::Metadata::new_from_path(file.path().unwrap());
    if let Ok(metadata) = metadata_res {
      println!("Removing metadata");
      let orientation = metadata.get_orientation();
      metadata.clear();
      metadata.set_orientation(orientation);
      metadata.save_to_file(file.path().unwrap()).unwrap();
    } else {
      println!("File not recognized, not removing")
    }
  }

  let filename = if let Some(input_name) = file.name() {
    format!("_{}", input_name)
  } else {
    String::new()
  };

  // TODO: get original extension
  let extension = if let Some(content_type) = file.content_type() {
    if let Some(extension) = content_type.extension() {
      format!(".{}", extension)
    } else {
      String::new()
    }
  } else {
    String::new()
  };

  let output_file = format!("{}{}{}", prefix, filename, extension);

  if output_file.contains('/') {
    return Err((Status::BadRequest, "Filename cannot contain a slash"))
  }

  let output_path = env::current_dir().unwrap().join("uploads").join(output_file.clone());

  let tmp_persist = file.persist_to(output_path.clone()).await;

  if tmp_persist.is_err() {
    println!("WARN: Cannot efficiently persist across filesystems");
    file.move_copy_to(output_path).await.unwrap();
  }

  Ok(Json(UploadResult {
    file_path: format!("/~/{}", output_file),
    retention_hours: crate::cleanup::get_retention_hours(file.len())
  }))
}
