use std::path::Path;

use rocket::{fs::{NamedFile, relative}, response::Redirect};

use crate::sessions::{Session};

#[get("/style.css")]
pub async fn style_file() -> NamedFile {
  NamedFile::open(Path::new(relative!("./ui/style.css"))).await.unwrap()
}


#[get("/", rank = 1)]
pub async fn index_authenticated(_session : Session) -> NamedFile {
  NamedFile::open(Path::new(relative!("./ui/index.html"))).await.unwrap()
}

#[get("/", rank = 2)]
pub async fn index_unauthenticated() -> Redirect {
  Redirect::temporary("/login")
}


#[get("/login", rank = 2)]
pub async fn login_unauthenticated() -> NamedFile {
  NamedFile::open(Path::new(relative!("./ui/login.html"))).await.unwrap()
}

#[get("/login", rank = 1)]
pub async fn login_authenticated(_session : Session) -> Redirect{
  Redirect::temporary("/")
}