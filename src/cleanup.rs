use chrono::prelude::*;
use std::fs::{read_dir, remove_file};
use std::path::Path;
use std::time::SystemTime;
use std::{thread, time::Duration};

use crate::sessions::Sessions;

pub const MAX_SIZE: u32 = 1500000000;
pub const HOURS_MAX: u32 = 72;
pub const HOURS_MIN: u32 = 3;

pub fn start_loop(sessions: Sessions) {
    thread::sleep(Duration::from_secs(30));

    loop {
        let time = Local::now();
        println!("Starting cleanup at {}", time.to_rfc2822());
        cleanup_sessions(&sessions);
        cleanup_files();

        thread::sleep(Duration::from_secs(5 * 60));
    }
}

pub fn get_retention_hours(file_size: u64) -> u64 {
    let retention_hours = HOURS_MIN as f64
        + (HOURS_MIN as i64 - HOURS_MAX as i64) as f64
            * (file_size as f64 / MAX_SIZE as f64 - 1.0).powf(7.0);
    return retention_hours as u64;
}

fn cleanup_sessions(sessions: &Sessions) {
    let num_before = sessions.len();
    sessions.cleanup();
    let num_after = sessions.len();

    let removed = num_before - num_after;

    if num_before > 0 {
        println!("- Removed {} of {} sessions", removed, num_before);
    }
}

fn cleanup_files() {
    let files = read_dir(Path::new("./uploads")).unwrap();

    for file_result in files {
        if file_result.is_err() {
            println!("Cannot open file");
            continue;
        }

        let file = file_result.unwrap();
        let metadata = file.metadata().unwrap();
        let file_size = metadata.len();

        let file_age_seconds = SystemTime::now()
            .duration_since(metadata.created().unwrap())
            .unwrap()
            .as_secs();
        let file_age_hours = file_age_seconds as u64 / 60 / 60;
        let retention_hours = get_retention_hours(file_size as u64);

        if file_age_hours > retention_hours {
            if let Err(_err) = remove_file(file.path()) {
                println!("- Error removing file {}", file.path().display());
            } else {
                println!(
                    "- Removed {} ({} bytes, {} hours old)",
                    file.path().display(),
                    file_size,
                    file_age_hours
                );
            }
        } else {
            println!(
                "- Not removing {} ({} bytes) as it is not old enough ({}/{} hours)",
                file.path().display(),
                file_size,
                file_age_hours,
                retention_hours
            );
        }
    }
}
