#[macro_use]
extern crate rocket;

use std::{fs, path::Path, thread};
use rocket::fs::{FileServer, relative};

use crate::sessions::Sessions;

mod api;
mod ui;
mod sessions;
mod cleanup;

#[rocket::main]
async fn main() {
    let arg = std::env::args().nth(1);
    let second_arg = std::env::args().nth(2);

    let data_dir = Path::new("./data");
    if !data_dir.exists() {
        println!("Creating data directory");
        std::fs::create_dir(data_dir).unwrap();
    }

    let shadow_file = Path::new("./data/shadow");
    if !shadow_file.exists() {
        println!("Creating password file");
        std::fs::write(shadow_file, "").unwrap();
    }

    let sessions = Sessions::new();

    match arg {
        None => start(sessions).await,
        Some(str) => match str.as_str() {
            "start" => start(sessions).await,
            "add" => add(second_arg).await,
            "remove" => remove(second_arg).await,
            _ => panic!("Unknown command {}", str)
        }
    }
}

async fn start(sessions : Sessions) {
    let uploads_dir = Path::new("./uploads");

    if !uploads_dir.exists() {
        println!("Creating uploads directory");
        std::fs::create_dir(uploads_dir).unwrap();
    }

    let thread_sessions = sessions.clone();
    println!("Spawning cleanup thread");
    thread::spawn(|| {
        cleanup::start_loop(thread_sessions);
    });

    println!("Starting web server");
    let launch_result = rocket::build()
        .manage(sessions)
        .mount("/", routes![
            api::login,
            api::logout,
            api::upload,
            ui::index_authenticated,
            ui::index_unauthenticated,
            ui::login_authenticated,
            ui::login_unauthenticated,
            ui::style_file
        ])
        .mount("/", FileServer::from(relative!("ui/")))
        .mount("/~/", FileServer::from(relative!("uploads/")).rank(5))
        .launch();

    if let Err(e) = launch_result.await {
        drop(e);
    }
}

async fn add(arg : Option<String>) {
    remove(arg.clone()).await;

    let username = arg.unwrap();

    let pass = rpassword::read_password_from_tty(Some("Password: ")).unwrap();
    let hash = bcrypt::hash(pass, 12).unwrap();

    let line = format!("{}:{}", username, hash);

    let contents = fs::read_to_string("./data/shadow").unwrap();

    let mut lines = contents
        .split('\n')
        .collect::<Vec<&str>>();

    lines.push(&line);

    let contents = lines.join("\n");

    fs::write("./data/shadow", contents).unwrap();
}

async fn remove(arg : Option<String>) {
    if arg.is_none() {
        panic!("Misisng argument: username");
    }

    let username = arg.unwrap();

    if username.contains(":") {
        panic!("Username cannot have :");
    }

    let contents = fs::read_to_string("./data/shadow").unwrap();

    let updated = contents
        .split('\n')
        .filter(|s| -> bool {
            ! s.starts_with(format!("{}:", username).as_str())
        })
        .collect::<Vec<&str>>()
        .join("\n");

    fs::write("./data/shadow", updated).unwrap();
}