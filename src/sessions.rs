use std::{collections::HashMap, sync::{Arc, RwLock}};
use chrono::{DateTime, Duration, Utc};
use rocket::{State, http::CookieJar, outcome::try_outcome, request::{self, Request, FromRequest}};
use uuid::Uuid;

pub type SessionId = Uuid;

#[derive(Clone)]
pub struct Session {
    username: String,
    duration : Duration,
    start : DateTime::<Utc>,
    last_active : DateTime::<Utc>
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Session {
    type Error = ();

    async fn from_request(request : &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let cookies = request.guard::<&CookieJar<'_>>().await.unwrap();
        let sessions = try_outcome!(request.guard::<&State<Sessions>>().await);

        let cookie = cookies.get("session");

        if cookie.is_none() {
          return request::Outcome::Forward(())
        }

        let cookie_val = cookie.unwrap().value().parse::<SessionId>();

        if cookie_val.is_err() {
          return request::Outcome::Forward(())
        }

        let session_id = cookie.unwrap().value().parse::<SessionId>().unwrap();
        let session = sessions.get_session(session_id);

        if session.is_none() {
          return request::Outcome::Forward(());
        }

        return request::Outcome::Success(session.unwrap());
    }
}

#[derive(Clone)]
pub struct Sessions {
    sessions : Arc<RwLock<HashMap<SessionId, Session>>>
}

impl Sessions {
  pub fn new() -> Self {
    let map = Arc::new(RwLock::new(HashMap::new()));

    Sessions {
      sessions: map
    }
  }

  pub fn len(&self) -> usize {
    let guard = self.sessions.read().unwrap();
    let sessions = &*guard;

    sessions.len()
  }

  pub fn get_session(&self, id : SessionId) -> Option<Session> {
    let guard = self.sessions.read().unwrap();
    let sessions = &*guard;

    if !sessions.contains_key(&id) {
      return None
    }

    let session = sessions.get(&id).unwrap().clone();

    if session.start + session.duration < Utc::now() {
      return None
    }

    return Some(session)
  }

  pub fn remove_session(&self, id : SessionId) {
    let mut guard = self.sessions.write().unwrap();
    let sessions = &mut *guard;

    sessions.remove(&id);
  }

  pub fn create_session(&self, username: String, duration : Duration) -> Option<(SessionId, Session)> {
    let mut guard = self.sessions.write().unwrap();
    let sessions = &mut *guard;

    let id = Uuid::new_v4();

    if sessions.contains_key(&id) {
      return None
    }

    let now = Utc::now();
    let session = Session {
      username,
      duration,
      start: now.clone(),
      last_active: now.clone()
    };

    println!("   >> Creating session for {}", session.username);

    sessions.insert(id, session.clone());
    Some((id, session))
  }

  pub fn cleanup(&self) {
    let mut guard = self.sessions.write().unwrap();
    let sessions = &mut *guard;

    let keys = sessions.keys().map(|u| -> Uuid {*u}).collect::<Vec<SessionId>>();

    for key in keys {
      let session = sessions.get(&key).unwrap();

      if session.last_active + session.duration < Utc::now() {
        sessions.remove(&key);
      }
    }
  }
}

